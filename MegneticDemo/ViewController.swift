//
//  ViewController.swift
//  MegneticDemo
//
//  Created by Bhavin on 10/07/19.
//  Copyright © 2019 Logistic Infotech PVT LTD. All rights reserved.
//

import UIKit
import Accelerate
import CoreMotion

class ViewController: UIViewController {

    @IBOutlet weak var lblData: UILabel!
    var motionManager:CMMotionManager=CMMotionManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.detect()
    }

    func detect() {

        let queue:OperationQueue = OperationQueue()
        
        if motionManager.isDeviceMotionAvailable {
            motionManager.magnetometerUpdateInterval = 1
            motionManager.accelerometerUpdateInterval = 1
            motionManager.showsDeviceMovementDisplay = true
            
            
            motionManager.startMagnetometerUpdates(to: queue) { (data, error) in
                
                
                let data = sqrt((Double((data?.magneticField.x)!) * Double((data?.magneticField.x)!)) + ((Double((data?.magneticField.y)!) * Double((data?.magneticField.y)!))) + ((Double((data?.magneticField.z)!) * Double((data?.magneticField.z)!))))
                
                DispatchQueue.main.async {
                    self.lblData.text = "\(data/100)"
                }
            }
            
//            motionManager.startDeviceMotionUpdates(to: queue) { (deviceMotion, error) in
//
//                if let motion = deviceMotion {
//                    var accuracy = motion.magneticField.accuracy
//                    var x = motion.magneticField.field.x
//                    var y = motion.magneticField.field.y
//                    var z = motion.magneticField.field.z
//                    print("accuracy: \(accuracy.rawValue), x: \(x), y: \(y), z: \(z)")
//                }
//                else {
//                    print("Device motion is nil.")
//                }
//            }
            
//            motionManager.startDeviceMotionUpdates(using: CMAttitudeReferenceFrame.xArbitraryZVertical, to: queue) { (deviceMotion, error) in
//
//                if let motion = deviceMotion {
//                    var accuracy = motion.magneticField.accuracy
//                    var x = motion.magneticField.field.x
//                    var y = motion.magneticField.field.y
//                    var z = motion.magneticField.field.z
//                    print("accuracy: \(accuracy.rawValue), x: \(x), y: \(y), z: \(z)")
//                }
//                else {
//                    print("Device motion is nil.")
//                }
//
//            }
            
            
            
//            motionManager.startDeviceMotionUpdatesUsingReferenceFrame(CMAttitudeReferencCMAttitudeReferenceFrame.xArbitraryZVerticaleFrame, toQueue: queue, withHandler: {
//                (deviceMotion: CMDeviceMotion!, error: NSError!) -> Void in
//                // If no device-motion data is available, the value of this property is nil.
//                if let motion = deviceMotion {
//                    println(motion)
//                    var accuracy = motion.magneticField.accuracy
//                    var x = motion.magneticField.field.x
//                    var y = motion.magneticField.field.y
//                    var z = motion.magneticField.field.z
//                    println("accuracy: \(accuracy.value), x: \(x), y: \(y), z: \(z)")
//                }
//                else {
//                    println("Device motion is nil.")
//                }
//            })
        }
    }

}

